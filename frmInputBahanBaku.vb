﻿Imports MySql.Data.MySqlClient
Public Class frmInputBahanBaku
    Public kode As String
    Dim isEdit As Boolean

    Private Sub frmInputBahanBaku_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        frmLogin.Koneksi.isi_combobox_jenis_bahan_baku(cmbJenis)

        If kode <> "" Then
            Me.Text = "Edit Bahan Baku"
            isEdit = True
            txtKode.ReadOnly = True
            Dim dt As New DataTable
            Dim adapter As MySqlDataAdapter
            Try
                adapter = New MySqlDataAdapter(
                    "select kode, nama, jenis, min_stok from " &
                    "master_bahan_baku where kode='" &
                    kode & "'", frmLogin.Koneksi.Conn)
                adapter.Fill(dt)
                txtKode.Text = dt.Rows(0)(0).ToString
                txtNama.Text = dt.Rows(0)(1).ToString
                cmbJenis.Text = dt.Rows(0)(2).ToString
                txtMinStok.Text = dt.Rows(0)(3).ToString
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            Finally
                frmLogin.Koneksi.Conn.Close()
            End Try
        Else
            isEdit = False
        End If
    End Sub

    Private Sub btnBatal_Click(sender As Object, e As EventArgs) Handles btnBatal.Click
        Dispose()
    End Sub

    Private Sub txtMinStok_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMinStok.KeyPress
        If (Not Char.IsControl(e.KeyChar) And Not Char.IsDigit(e.KeyChar) And (e.KeyChar <> ".")) Then
            e.Handled = True
        End If
    End Sub

    Private Function validasi() As Boolean
        Dim dt As New DataTable
        Dim adapter As MySqlDataAdapter
        Dim filter As String

        If Trim(txtKode.Text) = "" Then
            MessageBox.Show("Kode belum diisi.")
            txtKode.Focus()
            Return False
            Exit Function
        End If

        Try
            If isEdit Then filter = " and upper(kode) <> upper('" &
                kode & "')"
            adapter = New MySqlDataAdapter(
                "select count(*) from " &
                "master_bahan_baku where upper(kode)= upper('" &
                txtKode.Text & "')" & filter, frmLogin.Koneksi.Conn)
            adapter.Fill(dt)
            If (Convert.ToInt32(dt.Rows(0)(0).ToString) = 0) Then
                Return True
            Else
                Return False
                MessageBox.Show("Kode sudah ada.")
                txtKode.Focus()
                Exit Function
            End If


        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return False
            Exit Function
        Finally
            frmLogin.Koneksi.Conn.Close()
        End Try

    End Function

    Private Sub btnSimpan_Click(sender As Object, e As EventArgs) Handles btnSimpan.Click
        If validasi() Then
            Dim result As DialogResult = MessageBox.Show("Apakah data sudah benar?", "Simpan", MessageBoxButtons.YesNo)
            If (result = DialogResult.Yes) Then
                If Not isEdit Then
                    frmLogin.Koneksi.save_master_bahan_baku(txtKode.Text, txtNama.Text, cmbJenis.Text, Convert.ToDouble(txtMinStok.Text))
                Else
                    frmLogin.Koneksi.update_master_bahan_baku(txtKode.Text, txtNama.Text, cmbJenis.Text, Convert.ToDouble(txtMinStok.Text))
                End If
                Dispose()
                frmRekapBahanBaku.load()
            End If
        End If
    End Sub
End Class