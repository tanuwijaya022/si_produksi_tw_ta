﻿Imports System.Globalization
Imports MySql.Data.MySqlClient
Public Class frmInputPermintaanPembelian
    Public seq As String
    Dim isEdit, isDetail As Boolean


    Public Sub execute(ByVal AisEdit As Boolean, ByVal Aisdetail As Boolean, ByVal ASeq As String)
        isEdit = AisEdit
        isDetail = Aisdetail
        seq = ASeq

        dtpTanggal.Value = frmLogin.Koneksi.get_date_now()
        txtNomor.Text = frmLogin.Koneksi.get_nomor_all_trans("PP", dtpTanggal.Value.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture))

        If isDetail Or isEdit Then
            load()
        End If

        Me.ShowDialog()
    End Sub

    Sub load()
        asgInput.AllowUserToDeleteRows = True
        asgInput.Rows.Clear()
        asgInput.RowCount = 1
        setAlignColumn()

        If isDetail Then
            Me.Text = "Detail Permintaan Pembelian"
            btnSimpan.Visible = False
        ElseIf isEdit Then
            Me.Text = "Edit Permintaan Pembelian"
        End If
        Dim dt As New DataTable
        Dim adapter As MySqlDataAdapter
        Try
            adapter = New MySqlDataAdapter(
                    "select seq, nomor, tanggal from " &
                    "permintaan_pembelian_master where seq='" &
                    seq & "'", frmLogin.Koneksi.Conn)
            adapter.Fill(dt)
            txtNomor.Text = dt.Rows(0)(1).ToString
            dtpTanggal.Value = dt.Rows(0)(2).ToString

            frmLogin.Koneksi.load_permintaan_pembelian_detail(asgInput, seq)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            frmLogin.Koneksi.Conn.Close()
        End Try
    End Sub
    Sub CariBahanBaku(ByVal str As String)
        Dim dt As New DataTable
        Dim adapter As MySqlDataAdapter
        Dim idLov As String
        Try
            adapter = New MySqlDataAdapter(
            "select kode,nama from master_bahan_baku " &
            " where upper(kode)= upper('" &
            str & "') and is_non_aktif = 'F' ", frmLogin.Koneksi.Conn)
            adapter.Fill(dt)

            If dt.Rows.Count = 0 Then
                idLov = frmLOV.execute("BB", str)
                adapter = New MySqlDataAdapter(
                "select kode,nama from master_bahan_baku " &
                " where upper(kode)= upper('" &
                idLov & "') and is_non_aktif = 'F' ", frmLogin.Koneksi.Conn)
                adapter.Fill(dt)
            End If


            asgInput(1, asgInput.CurrentCell.RowIndex).Value =
                dt.Rows(0)(0).ToString
            asgInput(2, asgInput.CurrentCell.RowIndex).Value =
                dt.Rows(0)(1).ToString

        Catch ex As Exception
            MessageBox.Show("Data tidak ditemukan", "Error")
            asgInput(1, asgInput.CurrentCell.RowIndex).Value = ""
            asgInput(2, asgInput.CurrentCell.RowIndex).Value = ""
            asgInput(3, asgInput.CurrentCell.RowIndex).Value = ""
        Finally
            frmLogin.Koneksi.Conn.Close()
        End Try
    End Sub

    Private Sub asgInput_RowsAdded(sender As Object, e As DataGridViewRowsAddedEventArgs) Handles asgInput.RowsAdded
        For i As Integer = 0 To asgInput.Rows.Count - 1
            asgInput(0, i).Value = i + 1
        Next
    End Sub

    Private Sub asgInput_UserDeletingRow(sender As Object, e As DataGridViewRowCancelEventArgs) Handles asgInput.UserDeletingRow
        If asgInput.RowCount = 1 Then
            asgInput.AllowUserToDeleteRows = False
            asgInput.Rows.Clear()
        Else
            asgInput.AllowUserToDeleteRows = True
        End If
    End Sub

    Private Sub asgInput_UserDeletedRow(sender As Object, e As DataGridViewRowEventArgs) Handles asgInput.UserDeletedRow
        If asgInput.RowCount = 1 Then
            asgInput.AllowUserToDeleteRows = False
        Else
            asgInput.AllowUserToDeleteRows = True
        End If
    End Sub

    Private Sub asgInput_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles asgInput.CellEndEdit
        If asgInput.CurrentCell.ColumnIndex = 1 Then
            CariBahanBaku(asgInput.CurrentCell.Value)
        End If
    End Sub

    Private Function validasi() As Boolean
        If (asgInput(1, 0).Value = "") Then
            MessageBox.Show("Data bahan baku belum diisi.")
            Return False
            Exit Function
        End If

        For i = 0 To asgInput.RowCount - 2

            For j = i + 1 To asgInput.RowCount - 2
                If asgInput(1, i).Value = asgInput(1, j).Value Then
                    MessageBox.Show("Kode baris ke " & (i + 1) & " sama dengan baris " & (j + 1) & vbCrLf & "Silahkan ubah salah satunya.")
                    Return False
                    Exit Function
                End If
            Next j
        Next i
        Return True
    End Function

    Private Sub asgInput_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles asgInput.CellEnter
        If (e.ColumnIndex = 0) Then


            asgInput.Columns(e.ColumnIndex).ReadOnly = True

        End If
    End Sub


    Private Sub ColJumlah_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = ".") Then
            e.Handled = True
        End If
    End Sub

    Private Sub Coltext_KeyPress(sender As Object, e As KeyPressEventArgs)
        e.Handled = False
    End Sub


    Sub setAlignColumn()

        asgInput.Columns(0).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgInput.Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgInput.Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgInput.Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter

        asgInput.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        asgInput.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

    End Sub

    Private Sub asgInput_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) Handles asgInput.EditingControlShowing
        If asgInput.CurrentCell.ColumnIndex = 3 Then

            AddHandler CType(e.Control, TextBox).KeyPress, AddressOf ColJumlah_KeyPress

        Else
            AddHandler CType(e.Control, TextBox).KeyPress, AddressOf Coltext_KeyPress
        End If
    End Sub
End Class