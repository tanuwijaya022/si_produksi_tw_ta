﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLOV
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLOV))
        Me.gr = New System.Windows.Forms.GroupBox()
        Me.btnCari = New System.Windows.Forms.Button()
        Me.txtCari = New System.Windows.Forms.TextBox()
        Me.asgRekap = New System.Windows.Forms.DataGridView()
        Me.btnPilih = New System.Windows.Forms.Button()
        Me.btnBatal = New System.Windows.Forms.Button()
        Me.gr.SuspendLayout()
        CType(Me.asgRekap, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gr
        '
        Me.gr.Controls.Add(Me.btnCari)
        Me.gr.Controls.Add(Me.txtCari)
        Me.gr.Location = New System.Drawing.Point(2, 3)
        Me.gr.Name = "gr"
        Me.gr.Size = New System.Drawing.Size(395, 56)
        Me.gr.TabIndex = 36
        Me.gr.TabStop = False
        Me.gr.Text = "Cari"
        '
        'btnCari
        '
        Me.btnCari.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnCari.FlatAppearance.BorderSize = 0
        Me.btnCari.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCari.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnCari.Location = New System.Drawing.Point(314, 22)
        Me.btnCari.Name = "btnCari"
        Me.btnCari.Size = New System.Drawing.Size(75, 23)
        Me.btnCari.TabIndex = 30
        Me.btnCari.Text = "&Cari"
        Me.btnCari.UseVisualStyleBackColor = False
        '
        'txtCari
        '
        Me.txtCari.Location = New System.Drawing.Point(16, 24)
        Me.txtCari.Name = "txtCari"
        Me.txtCari.Size = New System.Drawing.Size(292, 20)
        Me.txtCari.TabIndex = 29
        '
        'asgRekap
        '
        Me.asgRekap.AllowUserToAddRows = False
        Me.asgRekap.AllowUserToDeleteRows = False
        Me.asgRekap.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.asgRekap.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.asgRekap.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.asgRekap.Location = New System.Drawing.Point(6, 65)
        Me.asgRekap.Name = "asgRekap"
        Me.asgRekap.ReadOnly = True
        Me.asgRekap.Size = New System.Drawing.Size(391, 310)
        Me.asgRekap.TabIndex = 35
        '
        'btnPilih
        '
        Me.btnPilih.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnPilih.FlatAppearance.BorderSize = 0
        Me.btnPilih.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPilih.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnPilih.Location = New System.Drawing.Point(230, 381)
        Me.btnPilih.Name = "btnPilih"
        Me.btnPilih.Size = New System.Drawing.Size(75, 23)
        Me.btnPilih.TabIndex = 37
        Me.btnPilih.Text = "&Pilih"
        Me.btnPilih.UseVisualStyleBackColor = False
        '
        'btnBatal
        '
        Me.btnBatal.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnBatal.FlatAppearance.BorderSize = 0
        Me.btnBatal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBatal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnBatal.Location = New System.Drawing.Point(311, 381)
        Me.btnBatal.Name = "btnBatal"
        Me.btnBatal.Size = New System.Drawing.Size(75, 23)
        Me.btnBatal.TabIndex = 38
        Me.btnBatal.Text = "&Batal"
        Me.btnBatal.UseVisualStyleBackColor = False
        '
        'frmLOV
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(398, 416)
        Me.Controls.Add(Me.btnBatal)
        Me.Controls.Add(Me.btnPilih)
        Me.Controls.Add(Me.gr)
        Me.Controls.Add(Me.asgRekap)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmLOV"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "LOV"
        Me.gr.ResumeLayout(False)
        Me.gr.PerformLayout()
        CType(Me.asgRekap, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents gr As GroupBox
    Friend WithEvents btnCari As Button
    Friend WithEvents txtCari As TextBox
    Friend WithEvents asgRekap As DataGridView
    Friend WithEvents btnPilih As Button
    Friend WithEvents btnBatal As Button
End Class
