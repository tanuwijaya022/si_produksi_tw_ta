﻿Public Class frmRekapBahanBaku

    Public Const WM_NCLBUTTONDBLCLK As Integer = &HA3

    Public Sub load()
        frmLogin.Koneksi.load_rekap_master_bahan_baku(asgRekap, Trim(txtCari.Text))

    End Sub
    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
        If m.Msg = WM_NCLBUTTONDBLCLK Then Return
        MyBase.WndProc(m)
    End Sub
    Private Sub frmRekapBahanBaku_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        frmLogin.Koneksi.load_rekap_master_bahan_baku(asgRekap, Trim(txtCari.Text))
        setAlignColumn()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        frmInputBahanBaku.kode = ""
        frmInputBahanBaku.ShowDialog()
    End Sub

    Private Sub btnCari_Click(sender As Object, e As EventArgs) Handles btnCari.Click
        frmLogin.Koneksi.load_rekap_master_bahan_baku(asgRekap, Trim(txtCari.Text))
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        frmInputBahanBaku.kode = asgRekap(1, asgRekap.CurrentCell.RowIndex).Value
        frmInputBahanBaku.ShowDialog()
    End Sub

    Private Sub btnHapus_Click(sender As Object, e As EventArgs) Handles btnHapus.Click
        Dim result As DialogResult = MessageBox.Show("Apakah bahan baku '" & asgRekap(1, asgRekap.CurrentCell.RowIndex).Value & "' ingin dihapus?",
                              "Hapus",
                              MessageBoxButtons.YesNo)
        If (result = DialogResult.Yes) Then
            frmLogin.Koneksi.delete_master_bahan_baku(asgRekap(1, asgRekap.CurrentCell.RowIndex).Value)
            frmLogin.Koneksi.load_rekap_master_bahan_baku(asgRekap, Trim(txtCari.Text))
        End If
    End Sub


    Sub setAlignColumn()

        asgRekap.Columns(0).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekap.Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekap.Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekap.Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekap.Columns(4).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekap.Columns(5).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter

        asgRekap.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        asgRekap.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        asgRekap.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

    End Sub
End Class