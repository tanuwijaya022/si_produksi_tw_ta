﻿
Imports MySql.Data.MySqlClient
Public Class frmAddUser
    Public userid As String
    Dim isEdit As Boolean

    Private Function validasi() As Boolean
        Dim dt As New DataTable
        Dim adapter As MySqlDataAdapter
        Dim filter As String

        If Trim(txtUser.Text) = "" Then
            MessageBox.Show("User id belum diisi.")
            txtUser.Focus()
            Return False
            Exit Function
        End If

        Try
            If isEdit Then filter = " and upper(userid) <> upper('" &
                userid & "')"
            adapter = New MySqlDataAdapter(
                "select count(*) from " &
                "users where upper(userid)= upper('" &
                txtUser.Text & "')" & filter, frmLogin.Koneksi.Conn)
            adapter.Fill(dt)
            If (Convert.ToInt32(dt.Rows(0)(0).ToString) = 0) Then
                Return True
            Else
                MessageBox.Show("User id sudah ada.")
                Return False
            End If


        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return False
        Finally
            frmLogin.Koneksi.Conn.Close()
        End Try
    End Function
    Private Sub btnSimpan_Click(sender As Object, e As EventArgs) Handles btnSimpan.Click
        If validasi() Then
            Dim result As DialogResult = MessageBox.Show("Apakah data sudah benar?", "Simpan", MessageBoxButtons.YesNo)
            If (result = DialogResult.Yes) Then
                If Not isEdit Then
                    frmLogin.Koneksi.save_user(txtUser.Text, txtPass.Text)
                Else
                    frmLogin.Koneksi.edit_user(txtUser.Text, txtPass.Text)
                End If
                Dispose()
                frmRekapUser.load()
            End If
        End If
    End Sub

    Private Sub btnBatal_Click(sender As Object, e As EventArgs) Handles btnBatal.Click
        Dispose()
    End Sub

    Private Sub frmAddUser_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If userid <> "" Then
            Me.Text = "Edit User"
            txtUser.ReadOnly = True
            isEdit = True
            Dim dt As New DataTable
            Dim adapter As MySqlDataAdapter
            Try
                adapter = New MySqlDataAdapter(
                    "select userid, password from " &
                    "users where userid='" &
                    userid & "'", frmLogin.Koneksi.Conn)
                adapter.Fill(dt)
                txtUser.Text = dt.Rows(0)(0).ToString
                txtPass.Text = dt.Rows(0)(1).ToString
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            Finally
                frmLogin.Koneksi.Conn.Close()
            End Try
        Else
            isEdit = False
            txtPass.Clear()
            txtUser.Clear()
        End If
    End Sub
End Class