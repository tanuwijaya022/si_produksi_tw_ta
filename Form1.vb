﻿Imports System.Globalization

Public Class frmLogin
    Public Koneksi As koneksi
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        If Koneksi.cekLogin(txtUser.Text, txtPass.Text) Then
            Me.Hide()
            frmMenuUtama.Show()
            'frmMenuUtama.Load(e)
        Else
            MsgBox("Invalid username/Password.")
            txtUser.Focus()
        End If
    End Sub

    Public Sub load()
        txtPass.Clear()
        txtUser.Clear()
        txtUser.Focus()
        Koneksi.level = 0
    End Sub


    Private Sub frmLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Koneksi = New koneksi()
        txtUser.Select()
    End Sub



    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnBatal.Click
        Close()
    End Sub

    Private Sub txtPass_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPass.KeyPress

    End Sub

    Private Sub txtPass_KeyDown(sender As Object, e As KeyEventArgs) Handles txtPass.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            btnLogin.Focus()
        End If
    End Sub

    Private Sub txtUser_KeyDown(sender As Object, e As KeyEventArgs) Handles txtUser.KeyDown

        If e.KeyCode = Keys.Enter Then

            e.SuppressKeyPress = True
            txtPass.Focus()

        End If
    End Sub

End Class
