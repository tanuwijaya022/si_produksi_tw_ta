﻿Public Class frmBarang

    Public Const WM_NCLBUTTONDBLCLK As Integer = &HA3

    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
        If m.Msg = WM_NCLBUTTONDBLCLK Then Return
        MyBase.WndProc(m)
    End Sub

    Sub setAlignColumn()

        asgRekap.Columns(0).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekap.Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekap.Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekap.Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekap.Columns(4).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekap.Columns(5).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter

        asgRekap.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        asgRekap.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        asgRekap.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

    End Sub

    Public Sub Load()

        frmLogin.Koneksi.load_rekap_master_barang(asgRekap, Trim(txtCari.Text))
    End Sub
    Private Sub frmBarang_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        frmLogin.Koneksi.load_rekap_master_barang(asgRekap, Trim(txtCari.Text))
        setAlignColumn()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        frmTambahBarang.kode = ""
        frmTambahBarang.ShowDialog()
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        frmTambahBarang.kode = asgRekap(1, asgRekap.CurrentCell.RowIndex).Value
        frmTambahBarang.ShowDialog()
    End Sub

    Private Sub btnHapus_Click(sender As Object, e As EventArgs) Handles btnHapus.Click
        Dim result As DialogResult = MessageBox.Show("Apakah barang '" & asgRekap(1, asgRekap.CurrentCell.RowIndex).Value & "' ingin dihapus?",
                              "Hapus",
                              MessageBoxButtons.YesNo)
        If (result = DialogResult.Yes) Then
            frmLogin.Koneksi.delete_master_barang(asgRekap(1, asgRekap.CurrentCell.RowIndex).Value)
            Load()
        End If

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Load()
    End Sub

    Private Sub asgRekap_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles asgRekap.CellFormatting
        For Each row As DataGridViewRow In asgRekap.Rows
            If (row.Cells(4).Value > row.Cells(5).Value) And (row.Cells(4).Value > 0) Then
                row.DefaultCellStyle.ForeColor = Color.Red
            End If
        Next
    End Sub
End Class