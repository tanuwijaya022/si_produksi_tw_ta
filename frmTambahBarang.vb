﻿Imports MySql.Data.MySqlClient
Public Class frmTambahBarang
    Public kode As String
    Dim isEdit As Boolean

    Public Const WM_NCLBUTTONDBLCLK As Integer = &HA3

    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
        If m.Msg = WM_NCLBUTTONDBLCLK Then Return
        MyBase.WndProc(m)
    End Sub


    Private Sub txtMinStok_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMinStok.KeyPress
        If (Not Char.IsControl(e.KeyChar) And Not Char.IsDigit(e.KeyChar) And (e.KeyChar <> ".")) Then
            e.Handled = True
        End If
    End Sub

    Private Sub frmTambahBarang_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        asgInput.AllowUserToDeleteRows = True
        asgInput.Rows.Clear()
        rbtStghJadi.Checked = True
        asgInput.RowCount = 1
        setAlignColumn()

        If kode <> "" Then
            Me.Text = "Edit Barang"
            isEdit = True
            txtKode.ReadOnly = True
            Dim dt As New DataTable
            Dim adapter As MySqlDataAdapter
            Try
                adapter = New MySqlDataAdapter(
                    "select kode, nama, is_jadi, min_stok from " &
                    "master_barang where kode='" &
                    kode & "'", frmLogin.Koneksi.Conn)
                adapter.Fill(dt)
                txtKode.Text = dt.Rows(0)(0).ToString
                txtNama.Text = dt.Rows(0)(1).ToString
                rbtStghJadi.Checked = dt.Rows(0)(2).ToString = "F"
                rbtJadi.Checked = dt.Rows(0)(2).ToString = "T"
                txtMinStok.Text = dt.Rows(0)(3).ToString


                frmLogin.Koneksi.load_detil_spesifikasi_barang(asgInput, kode)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            Finally
                frmLogin.Koneksi.Conn.Close()
            End Try
        Else
            isEdit = False
        End If
    End Sub


    Sub CariBahanBaku(ByVal str As String)
        Dim dt As New DataTable
        Dim adapter As MySqlDataAdapter
        Dim nmTabel, tipe, idLov As String
        Try
            If rbtStghJadi.Checked Then
                nmTabel = "master_bahan_baku"
                tipe = "BB"
            Else
                nmTabel = "master_barang"
                tipe = "BRG"
            End If
            adapter = New MySqlDataAdapter(
                "select kode,nama from " & nmTabel &
                " where upper(kode)= upper('" &
                str & "') and is_non_aktif = 'F' ", frmLogin.Koneksi.Conn)
            adapter.Fill(dt)

            If dt.Rows.Count = 0 Then
                idLov = frmLOV.execute(tipe, str)
                adapter = New MySqlDataAdapter(
                "select kode,nama from " & nmTabel &
                " where upper(kode)= upper('" &
                idLov & "') and is_non_aktif = 'F' ", frmLogin.Koneksi.Conn)
                adapter.Fill(dt)
            End If


            asgInput(1, asgInput.CurrentCell.RowIndex).Value =
                dt.Rows(0)(0).ToString
            asgInput(2, asgInput.CurrentCell.RowIndex).Value =
                dt.Rows(0)(1).ToString

        Catch ex As Exception
            MessageBox.Show("Data tidak ditemukan", "Error")
            asgInput(1, asgInput.CurrentCell.RowIndex).Value = ""
            asgInput(2, asgInput.CurrentCell.RowIndex).Value = ""
            asgInput(3, asgInput.CurrentCell.RowIndex).Value = ""
        Finally
            frmLogin.Koneksi.Conn.Close()
        End Try
    End Sub

    Private Sub asgInput_RowsAdded(sender As Object, e As DataGridViewRowsAddedEventArgs) Handles asgInput.RowsAdded
        For i As Integer = 0 To asgInput.Rows.Count - 1
            asgInput(0, i).Value = i + 1
        Next
    End Sub

    Private Sub asgInput_UserDeletingRow(sender As Object, e As DataGridViewRowCancelEventArgs) Handles asgInput.UserDeletingRow
        If asgInput.RowCount = 1 Then
            asgInput.AllowUserToDeleteRows = False
            asgInput.Rows.Clear()
        Else
            asgInput.AllowUserToDeleteRows = True
        End If
    End Sub

    Private Sub asgInput_UserDeletedRow(sender As Object, e As DataGridViewRowEventArgs) Handles asgInput.UserDeletedRow
        If asgInput.RowCount = 1 Then
            asgInput.AllowUserToDeleteRows = False
        Else
            asgInput.AllowUserToDeleteRows = True
        End If
    End Sub

    Private Sub asgInput_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles asgInput.CellEndEdit
        If asgInput.CurrentCell.ColumnIndex = 1 Then
            CariBahanBaku(asgInput.CurrentCell.Value)
        End If
    End Sub

    Private Function validasi() As Boolean
        Dim dt As New DataTable
        Dim adapter As MySqlDataAdapter
        Dim filter As String

        If txtMinStok.Text = "" Then
            txtMinStok.Text = "0"
        End If

        If Trim(txtKode.Text) = "" Then
            MessageBox.Show("Kode belum diisi.")
            txtKode.Focus()
            Return False
            Exit Function
        End If

        If (asgInput(1, 0).Value = "") Then
            MessageBox.Show("Spesifikasi belum diisi.")
            Return False
            Exit Function
        End If


        Try
            If isEdit Then filter = " and upper(kode) <> upper('" &
                kode & "')"
            adapter = New MySqlDataAdapter(
                "select count(*) from " &
                "master_barang where upper(kode)= upper('" &
                txtKode.Text & "')" & filter, frmLogin.Koneksi.Conn)
            adapter.Fill(dt)
            If (Convert.ToInt32(dt.Rows(0)(0).ToString) <> 0) Then
                MessageBox.Show("Kode sudah ada.")
                txtKode.Focus()
                Return False
                Exit Function
            End If


        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return False
            Exit Function
        Finally
            frmLogin.Koneksi.Conn.Close()
        End Try

        For i = 0 To asgInput.RowCount - 2

            For j = i + 1 To asgInput.RowCount - 2
                If asgInput(1, i).Value = asgInput(1, j).Value Then
                    MessageBox.Show("Kode baris ke " & (i + 1) & " sama dengan baris " & (j + 1) & vbCrLf & "Silahkan ubah salah satunya.")
                    Return False
                    Exit Function
                End If
            Next j
        Next i
        Return True
    End Function

    Private Sub asgInput_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles asgInput.CellEnter
        If (e.ColumnIndex = 0) Then


            asgInput.Columns(e.ColumnIndex).ReadOnly = True

        End If
    End Sub


    Private Sub ColJumlah_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = ".") Then
            e.Handled = True
        End If
    End Sub

    Private Sub Coltext_KeyPress(sender As Object, e As KeyPressEventArgs)
        e.Handled = False
    End Sub


    Sub setAlignColumn()

        asgInput.Columns(0).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgInput.Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgInput.Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgInput.Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter

        asgInput.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        asgInput.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

    End Sub

    Private Sub asgInput_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) Handles asgInput.EditingControlShowing
        If asgInput.CurrentCell.ColumnIndex = 3 Then

            AddHandler CType(e.Control, TextBox).KeyPress, AddressOf ColJumlah_KeyPress

        Else
            AddHandler CType(e.Control, TextBox).KeyPress, AddressOf Coltext_KeyPress
        End If
    End Sub

    Private Sub btnBatal_Click_1(sender As Object, e As EventArgs) Handles btnBatal.Click
        Dispose()
    End Sub

    Private Sub btnSimpan_Click_1(sender As Object, e As EventArgs) Handles btnSimpan.Click
        Dim is_jadi As String
        If rbtJadi.Checked Then
            is_jadi = "T"
        Else is_jadi = "F"
        End If
        If validasi() Then
            Dim result As DialogResult = MessageBox.Show("Apakah data sudah benar?", "Simpan", MessageBoxButtons.YesNo)
            If (result = DialogResult.Yes) Then
                frmLogin.Koneksi.Conn.Open()
                Dim myCommand As MySqlCommand = frmLogin.Koneksi.Conn.CreateCommand()
                Dim transaction As MySqlTransaction
                transaction = frmLogin.Koneksi.Conn.BeginTransaction()
                myCommand.Connection = frmLogin.Koneksi.Conn
                myCommand.Transaction = transaction
                If Not isEdit Then
                    Try
                        frmLogin.Koneksi.save_master_barang(txtKode.Text, txtNama.Text, is_jadi, Convert.ToDouble(txtMinStok.Text), myCommand)

                        For i = 0 To asgInput.RowCount - 2
                            frmLogin.Koneksi.save_detil_spesifikasi_barang(txtKode.Text, asgInput(1, i).Value, Convert.ToDouble(asgInput(3, i).Value.ToString), myCommand)
                        Next i


                        transaction.Commit()
                        'frmLogin.Koneksi.eksekusi("COMMIT;")
                        MessageBox.Show("Data barang berhasil disimpan.")
                    Catch ex As Exception
                        transaction.Rollback()
                        MessageBox.Show("Data barang gagal disimpan.")
                    End Try
                Else
                    Try
                        frmLogin.Koneksi.delete_detil_spesifikasi_barang(txtKode.Text, myCommand)
                        frmLogin.Koneksi.update_master_barang(txtKode.Text, txtNama.Text, is_jadi, Convert.ToDouble(txtMinStok.Text), myCommand)

                        For i = 0 To asgInput.RowCount - 2
                            If asgInput(1, i).Value <> Trim("") Then
                                frmLogin.Koneksi.save_detil_spesifikasi_barang(txtKode.Text, asgInput(1, i).Value, Convert.ToDouble(asgInput(3, i).Value.ToString), myCommand)
                            End If
                        Next i

                        transaction.Commit()
                        'frmLogin.Koneksi.eksekusi("COMMIT;")
                        MessageBox.Show("Data barang berhasil diedit.")
                    Catch ex As Exception
                        transaction.Rollback()
                        MessageBox.Show("Data barang gagal diedit.")
                    End Try
                End If

                Dispose()
                frmBarang.Load()
            End If
        End If
    End Sub
End Class