﻿Public Class frmLOV
    Dim tipe As String = ""

    Sub Load()
        frmLogin.Koneksi.load_lov_master(asgRekap, txtCari.Text, tipe)
    End Sub

    Sub setAlignColumn()

        asgRekap.Columns(0).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekap.Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekap.Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter

        asgRekap.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

    End Sub

    Private Sub LOV_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        setAlignColumn()
    End Sub

    Public Function execute(ByVal Atipe As String, ByVal cari As String) As String
        tipe = Atipe
        If tipe = "BB" Then
            Me.Text = "Daftar Bahan Baku"
        End If
        If tipe = "BRG" Then
            Me.Text = "Daftar Barang"
        End If
        txtCari.Text = cari
        Load()
        Me.ShowDialog()
        If DialogResult = DialogResult.OK Then
            Return asgRekap(1, asgRekap.CurrentCell.RowIndex).Value
        Else
            Return ""
        End If
    End Function

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btnCari.Click
        Load()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnPilih.Click
        DialogResult = DialogResult.OK
    End Sub

    Private Sub btnBatal_Click(sender As Object, e As EventArgs) Handles btnBatal.Click

        DialogResult = DialogResult.Cancel
    End Sub
End Class