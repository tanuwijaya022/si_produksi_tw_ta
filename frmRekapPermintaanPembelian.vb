﻿Imports MySql.Data.MySqlClient
Public Class frmRekapPermintaanPembelian
    Public Const WM_NCLBUTTONDBLCLK As Integer = &HA3

    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
        If m.Msg = WM_NCLBUTTONDBLCLK Then Return
        MyBase.WndProc(m)
    End Sub
    Private Sub frmRekapPermintaanPembelian_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtpDari.Value = frmLogin.Koneksi.get_date_now()
        dtpSampai.Value = frmLogin.Koneksi.get_date_now()
        load()
    End Sub

    Public Sub load()
        frmLogin.Koneksi.load_rekap_permintaan_pembelian_master(asgRekap, dtpDari.Value, dtpSampai.Value, Trim(txtCari.Text))
        setAlignColumn()
    End Sub

    Sub setAlignColumn()

        asgRekap.Columns(0).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekap.Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekap.Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekap.Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter

        asgRekap.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    End Sub


    Private Sub txtCari_KeyDown(sender As Object, e As KeyEventArgs) Handles txtCari.KeyDown

        e.SuppressKeyPress = True

        If e.KeyCode = Keys.Enter Then
            Dim dt As New DataTable
            Dim adapter As MySqlDataAdapter
            Dim idLov As String
            Try
                adapter = New MySqlDataAdapter(
                "select kode,nama from master_bahan_baku " &
                " where upper(kode)= upper('" &
                Trim(txtCari.Text) & "') and is_non_aktif = 'F' ", frmLogin.Koneksi.Conn)
                adapter.Fill(dt)

                If dt.Rows.Count = 0 Then
                    idLov = frmLOV.execute("BB", txtCari.Text)
                    adapter = New MySqlDataAdapter(
                    "select kode,nama from master_bahan_baku " &
                    " where upper(kode)= upper('" &
                    idLov & "') and is_non_aktif = 'F' ", frmLogin.Koneksi.Conn)
                    adapter.Fill(dt)
                End If


                txtCari.Text = dt.Rows(0)(0).ToString

            Catch ex As Exception
                MessageBox.Show("Data tidak ditemukan", "Error")
                txtCari.Text = ""
            Finally
                frmLogin.Koneksi.Conn.Close()
            End Try

            btncari.Focus()
        End If
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        frmInputPermintaanPembelian.execute(False, False, 0)
    End Sub
End Class