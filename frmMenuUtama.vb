﻿Imports System.Drawing
Imports System.IO
Imports System.Net
Imports System.Linq
Public Class frmMenuUtama
    Dim NotLogout As Boolean
    Private Function cekForm() As Boolean


        If Application.OpenForms().OfType(Of frmBarang).Any Then
            Return True
        Else
            Return False
        End If


    End Function
    Private Sub LogoutToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Me.Dispose()
        frmLogin.Show()
        frmLogin.load()
    End Sub

    Private Sub tabCreateUser_Click(sender As Object, e As EventArgs)
        frmAddUser.Show()
    End Sub

    Private Sub frmMenuUtama_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        NotLogout = True
        If frmLogin.Koneksi.level = 1000 Then
            lblLevelUser.Text = "Administrator"
            addButtonBarang()
            addButtonMasterBahanBaku()
            addButtonUser()
            addButtonMenuMaster()
        Else
            lblLevelUser.Text = "Produksi"
            addButtonTransPP()
            addButtonMenuTransaksi()
        End If

        'addImageUser()
    End Sub


    Dim btnTransPP As Windows.Forms.Button
    Private Sub addButtonTransPP()
        btnTransPP = New Windows.Forms.Button
        btnTransPP.Text = "Permintaan Pembelian"
        btnTransPP.Location = New Point(0, 200)
        btnTransPP.Dock = DockStyle.Top
        btnTransPP.FlatStyle = FlatStyle.Flat
        btnTransPP.FlatAppearance.BorderSize = 0
        btnTransPP.ForeColor = Color.White
        btnTransPP.Font = New Font(btnTransPP.Font, FontStyle.Regular)
        Panel3.Controls.Add(btnTransPP)
        AddHandler btnTransPP.Click, AddressOf btnTransPP_click

    End Sub

    Friend Sub btnTransPP_click(sender As Object, e As EventArgs)
        frmRekapPermintaanPembelian.Dispose()
        Panel4.Controls.Clear()
        frmRekapPermintaanPembelian.TopLevel = False
        Panel4.Controls.Add(frmRekapPermintaanPembelian)
        frmRekapPermintaanPembelian.Show()
    End Sub


    Private Sub addButtonMenuTransaksi()
        Dim b11 = New Windows.Forms.Button
        b11.Text = "Transaksi"
        b11.Location = New Point(0, 200)
        b11.Dock = DockStyle.Top
        b11.FlatStyle = FlatStyle.Flat
        b11.FlatAppearance.BorderSize = 1
        b11.ForeColor = Color.White
        b11.Font = New Font(b11.Font, FontStyle.Bold)
        Panel3.Controls.Add(b11)
        AddHandler b11.Click, AddressOf menuTransaksi_click
    End Sub

    Friend Sub menuTransaksi_click(sender As Object, e As EventArgs)
        If btnTransPP.Visible Then
            btnTransPP.Visible = False
        Else
            btnTransPP.Visible = True
        End If
    End Sub

    Private Sub addButtonMenuMaster()
        Dim b11 = New Windows.Forms.Button
        b11.Text = "Master"
        b11.Location = New Point(0, 200)
        b11.Dock = DockStyle.Top
        b11.FlatStyle = FlatStyle.Flat
        b11.FlatAppearance.BorderSize = 1
        b11.ForeColor = Color.White
        b11.Font = New Font(b11.Font, FontStyle.Bold)
        Panel3.Controls.Add(b11)
        AddHandler b11.Click, AddressOf menuMaster_click
    End Sub

    Friend Sub menuMaster_click(sender As Object, e As EventArgs)
        If btnUser.Visible Then
            btnUser.Visible = False
            bBrg.Visible = False
            btnBahanBaku.Visible = False
        Else
            btnUser.Visible = True
            bBrg.Visible = True
            btnBahanBaku.Visible = True
        End If
    End Sub

    Dim btnUser As Windows.Forms.Button
    Private Sub addButtonUser()
        btnUser = New Windows.Forms.Button
        btnUser.Text = "Users"
        btnUser.Location = New Point(0, 200)
        btnUser.Dock = DockStyle.Top
        btnUser.FlatStyle = FlatStyle.Flat
        btnUser.FlatAppearance.BorderSize = 0
        btnUser.ForeColor = Color.White
        btnUser.Font = New Font(btnUser.Font, FontStyle.Regular)
        Panel3.Controls.Add(btnUser)
        AddHandler btnUser.Click, AddressOf addUser_click

    End Sub



    Dim btnBahanBaku As Windows.Forms.Button
    Private Sub addButtonMasterBahanBaku()
        btnBahanBaku = New Windows.Forms.Button
        btnBahanBaku.Text = "Bahan Baku"
        btnBahanBaku.Location = New Point(0, 200)
        btnBahanBaku.Dock = DockStyle.Top
        btnBahanBaku.FlatStyle = FlatStyle.Flat
        btnBahanBaku.FlatAppearance.BorderSize = 0
        btnBahanBaku.ForeColor = Color.White
        btnBahanBaku.Font = New Font(btnBahanBaku.Font, FontStyle.Regular)
        Panel3.Controls.Add(btnBahanBaku)
        AddHandler btnBahanBaku.Click, AddressOf btnBahanBaku_click

    End Sub

    Friend Sub btnBahanBaku_click(sender As Object, e As EventArgs)
        frmRekapBahanBaku.Dispose()
        Panel4.Controls.Clear()
        frmRekapBahanBaku.TopLevel = False
        Panel4.Controls.Add(frmRekapBahanBaku)
        frmRekapBahanBaku.Show()
    End Sub

    Dim bBrg As Windows.Forms.Button
    Private Sub addButtonBarang()
        bBrg = New Windows.Forms.Button
        bBrg.Text = "Barang"
        bBrg.Location = New Point(0, 200 + 20)
        bBrg.Dock = DockStyle.Top
        bBrg.FlatStyle = FlatStyle.Flat
        bBrg.FlatAppearance.BorderSize = 0
        bBrg.ForeColor = Color.White
        'bBrg.Font = New Font(bBrg.Font, FontStyle.Bold)
        Panel3.Controls.Add(bBrg)
        AddHandler bBrg.Click, AddressOf addBarang_click

    End Sub

    Friend Sub addUser_click(sender As Object, e As EventArgs)
        frmRekapUser.Dispose()
        Panel4.Controls.Clear()
        frmRekapUser.TopLevel = False
        Panel4.Controls.Add(frmRekapUser)
        frmRekapUser.Show()
    End Sub

    Friend Sub addBarang_click(sender As Object, e As EventArgs)
        frmBarang.Dispose()
        Panel4.Controls.Clear()
        frmBarang.TopLevel = False
        Panel4.Controls.Add(frmBarang)
        frmBarang.Show()
    End Sub


    'Private Sub addImageUser()
    ' If frmLogin.Koneksi.level = 1000 Then
    ' Try
    ' Dim G As Graphics = imgFoto.CreateGraphics
    '
    '    Dim wc As New WebClient
    '    Dim bytes() As Byte = wc.DownloadData("https://images.all-free-download.com/images/graphicthumb/user_administrator_blue_100960.jpg")
    '    Dim ImgStream As New MemoryStream(bytes)'

    '                imgFoto.Image = Image.FromStream(ImgStream)
    '              'G.DrawImage(Image.FromStream(ImgStream), New Rectangle(0, 0, imgFoto.Width, imgFoto.Height))
    '            imgFoto.BringToFront()
    'Catch ex As Exception
    '          MsgBox(ex.Message)
    'End Try
    'End If
    'End Sub


    Private Sub btnLogout_Click(sender As Object, e As EventArgs) Handles btnLogout.Click
        NotLogout = False
        Me.Dispose()
        frmLogin.Show()
        frmLogin.load()
    End Sub

    Private Sub frmMenuUtama_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If NotLogout Then
            frmLogin.Dispose()
        End If
    End Sub

End Class