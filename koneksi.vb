﻿
Imports MySql.Data.MySqlClient
Public Class koneksi

    Public Conn As MySqlConnection
    Private Connstr As String
    Public level As Integer
    Dim adapter As MySqlDataAdapter

    Public Sub New()
        Connstr = "server=sql12.freemysqlhosting.net;" _
            & "port=3306;" _
            & "user=sql12384047;" _
            & "password='L9uD2zwuql';" _
            & "database=sql12384047"
        Conn = New MySqlConnection(Connstr)
        Conn.Open()
    End Sub

    Public Sub eksekusi(ByVal str1 As String, Optional isMessage As String = "")

        Dim adapter As MySqlDataAdapter
        Dim dt As New DataTable
        Try
            adapter = New MySqlDataAdapter(str1, Me.Conn)
            adapter.Fill(dt)
            If isMessage <> "" Then
                MsgBox(isMessage, MsgBoxStyle.Information, "Informasi")
            End If
        Catch ex As Exception
            MsgBox(ex.Message.ToString, MsgBoxStyle.Information, "Pesan")

        Finally
            Me.Conn.Close()
        End Try

    End Sub

    Public Function cekLogin(ByVal user As String, ByVal pass As String) As Boolean
        Dim adapter As MySqlDataAdapter
        Dim dt As New DataTable
        If (user = "Admin") And (pass = "admin") Then
            level = 1000
            Return True
        Else
            Try

                adapter = New MySqlDataAdapter(
            "select count(*) from users " &
            "where userid='" & user & "' and password='" & pass & "'",
            Me.Conn)
                adapter.Fill(dt)
                If dt.Rows(0)(0).ToString() <> 0 Then
                    level = 1
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                'MessageBox.Show("Data tidak ditemukan...", "Error")
                Return False
            Finally
                Me.Conn.Close()
            End Try
        End If
    End Function

    Public Sub save_user(ByVal user As String, ByVal pass As String)

        eksekusi("insert into users (userid,password) values ('" _
                 & user & "','" _
                 & pass & "'" _
                 & ")", "Data User berhasil disimpan.")
    End Sub

    Public Sub edit_user(ByVal user As String, ByVal pass As String)

        eksekusi("update users set password = '" & pass & "' " _
                 & " where userid = '" & user & "'", "Data User berhasil diedit.")
    End Sub

    Public Sub delete_user(ByVal user As String)
        eksekusi("delete from users where userid = '" & user & "'", "Data User berhasil dihapus.")
    End Sub

    Public Sub isi_combobox_jenis_bahan_baku(ByVal cmb As ComboBox)
        Dim dt As New DataTable
        Try
            adapter = New MySqlDataAdapter(
                "select distinct jenis from " &
                "master_bahan_baku order by jenis", Me.Conn)
            adapter.Fill(dt)
            cmb.Items.Clear()
            For i = 0 To dt.Rows.Count - 1
                cmb.Items.Add(dt.Rows(i)(0).ToString)
            Next i
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Me.Conn.Close()
        End Try
    End Sub



    Public Sub save_master_barang(ByVal kdBrg As String, ByVal nmBrg As String, ByVal is_jadi As String, ByVal min_stok As Double, ByVal myCommand As MySqlCommand)

        myCommand.CommandText = "insert into master_barang (kode,nama,is_jadi,min_stok,stok) values ('" _
                 & kdBrg & "','" _
                 & nmBrg & "','" _
                 & is_jadi & "'," _
                 & min_stok & ",0 " _
                 & ")"
        myCommand.ExecuteNonQuery()



    End Sub

    Public Sub update_master_barang(ByVal kdBrg As String, ByVal nmBrg As String, ByVal is_jadi As String, ByVal min_stok As Double, ByVal myCommand As MySqlCommand)

        myCommand.CommandText = "update master_barang set nama = '" & nmBrg & "'," _
                 & " is_jadi = '" & is_jadi & "'," _
                 & " min_stok = " & min_stok _
                 & " where kode = '" & kdBrg & "'"
        myCommand.ExecuteNonQuery()
    End Sub

    Public Sub delete_master_barang(ByVal kdBrg As String)
        eksekusi("update master_barang set is_non_aktif = 'T' where kode = '" & kdBrg & "'", "Data barang berhasil dihapus.")
    End Sub
    Public Sub delete_master_barang_permanen(ByVal kdBrg As String)
        eksekusi("delete master_barang where kode = '" & kdBrg & "'", "")
    End Sub


    Public Sub save_detil_spesifikasi_barang(ByVal kode_brg As String, ByVal kode_bb As String, ByVal qty As Double, ByVal myCommand As MySqlCommand)

        myCommand.CommandText = "insert into detil_spesifikasi_barang (kode_brg, kode_bb, qty) values ('" _
                 & kode_brg & "','" _
                 & kode_bb & "'," _
                 & qty _
                 & ")"
        myCommand.ExecuteNonQuery()
    End Sub

    Public Sub delete_detil_spesifikasi_barang(ByVal kode_brg As String, ByVal myCommand As MySqlCommand)
        myCommand.CommandText = "delete from detil_spesifikasi_barang where kode_brg = '" & kode_brg & "'"
        myCommand.ExecuteNonQuery()
    End Sub

    Public Sub load_rekap_user(ByVal asg As DataGridView, ByVal cari As String)

        Dim dt As New DataTable
        Dim filter As String

        filter = ""
        cari = Trim(cari)
        If cari <> "" Then
            filter = " where ((userid like '%" & cari & "%') or (nama like '%" & cari & "%'))"
        End If
        Try
            adapter = New MySqlDataAdapter("select 0, userid, nama, password from users " & filter, Me.Conn)
            adapter.Fill(dt)
            asg.DataSource = dt
            'Mendapatkan BindingManagerBase
            asg.Columns(0).HeaderText = "No."
            asg.Columns(1).HeaderText = "User Id"
            asg.Columns(2).HeaderText = "Nama"
            asg.Columns(3).HeaderText = "Password"


            For i As Integer = 0 To asg.Rows.Count - 1
                asg(0, i).Value = i + 1
            Next


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        Finally
            Me.Conn.Close()
        End Try
    End Sub



    Public Sub save_master_bahan_baku(ByVal kode As String, ByVal nama As String, ByVal jenis As String, ByVal min_stok As Double)

        eksekusi("insert into master_bahan_baku (kode, nama, jenis, min_stok, stok) values ('" _
                 & kode & "','" _
                 & nama & "', upper('" _
                 & jenis & "')," _
                 & min_stok & "," _
                 & "0" _
                 & ")", "Data bahan baku berhasil disimpan.")
    End Sub

    Public Sub update_master_bahan_baku(ByVal kode As String, ByVal nama As String, ByVal jenis As String, ByVal min_stok As Double)

        eksekusi("update master_bahan_baku set " _
                 & " nama = '" & nama & "'," _
                 & " min_stok = " & min_stok & "," _
                 & " jenis = upper('" & jenis & "')" _
                 & " where kode = '" & kode & "'", "Data bahan baku berhasil diupdate.")
    End Sub

    Public Sub delete_master_bahan_baku(ByVal kode As String)
        eksekusi("update master_bahan_baku set is_non_aktif = 'T' where kode = '" & kode & "'", "Data bahan baku berhasil dihapus.")
    End Sub

    Public Sub load_rekap_master_bahan_baku(ByVal asg As DataGridView, ByVal cari As String)

        Dim dt As New DataTable
        Dim filter As String

        filter = ""
        cari = Trim(cari)
        If cari <> "" Then
            filter = " and ((upper(kode) like upper('%" & cari & "%')) or (upper(nama) like upper('%" & cari & "%')))"
        End If
        Try
            adapter = New MySqlDataAdapter("select 0, kode, nama, jenis, min_stok, stok from master_bahan_baku where is_non_aktif = 'F' " & filter, Me.Conn)
            adapter.Fill(dt)
            asg.DataSource = dt
            'Mendapatkan BindingManagerBase
            asg.Columns(0).HeaderText = "No."
            asg.Columns(1).HeaderText = "Kode"
            asg.Columns(2).HeaderText = "Nama"
            asg.Columns(3).HeaderText = "Jenis"
            asg.Columns(4).HeaderText = "Min. Stok"
            asg.Columns(5).HeaderText = "Stok"

            For i As Integer = 0 To asg.Rows.Count - 1
                asg(0, i).Value = i + 1
            Next

        Catch ex As Exception
            MessageBox.Show(ex.Message)

        Finally
            Me.Conn.Close()
        End Try
    End Sub

    Public Sub load_lov_master(ByVal asg As DataGridView, ByVal cari As String, ByVal tipe As String)
        Dim dt As New DataTable
        Dim filter, nmTabel As String

        If tipe = "BB" Then
            nmTabel = "master_bahan_baku"
        End If

        If tipe = "BRG" Then
            nmTabel = "master_barang"
        End If

        filter = ""
        cari = Trim(cari)
        If cari <> "" Then
            filter = " and ((upper(kode) like upper('%" & cari & "%')) or (upper(nama) like upper('%" & cari & "%')))"
        End If
        Try
            adapter = New MySqlDataAdapter("select 0, kode, nama from " & nmTabel & " where is_non_aktif = 'F' " & filter, Me.Conn)
            adapter.Fill(dt)
            asg.DataSource = dt
            'Mendapatkan BindingManagerBase
            asg.Columns(0).HeaderText = "No."
            asg.Columns(1).HeaderText = "Kode"
            asg.Columns(2).HeaderText = "Nama"

            For i As Integer = 0 To asg.Rows.Count - 1
                asg(0, i).Value = i + 1
            Next

        Catch ex As Exception
            MessageBox.Show(ex.Message)

        Finally
            Me.Conn.Close()
        End Try
    End Sub

    Public Sub load_rekap_master_barang(ByVal asg As DataGridView, ByVal cari As String)

        Dim dt As New DataTable
        Dim filter As String

        filter = ""
        cari = Trim(cari)
        If cari <> "" Then
            filter = " and ((upper(kode) like upper('%" & cari & "%')) or (upper(nama) like upper('%" & cari & "%')))"
        End If
        Try
            adapter = New MySqlDataAdapter("select 0, kode, nama, IF(is_jadi='T', 'Barang Jadi', 'Barang Setengah Jadi'), min_stok, stok from master_barang where is_non_aktif = 'F' " & filter, Me.Conn)
            adapter.Fill(dt)
            asg.DataSource = dt
            'Mendapatkan BindingManagerBase
            asg.Columns(0).HeaderText = "No."
            asg.Columns(1).HeaderText = "Kode"
            asg.Columns(2).HeaderText = "Nama"
            asg.Columns(3).HeaderText = "Jenis"
            asg.Columns(4).HeaderText = "Min. Stok"
            asg.Columns(5).HeaderText = "Stok"

            For i As Integer = 0 To asg.Rows.Count - 1
                asg(0, i).Value = i + 1
            Next

        Catch ex As Exception
            MessageBox.Show(ex.Message)

        Finally
            Me.Conn.Close()
        End Try
    End Sub

    Public Sub load_detil_spesifikasi_barang(ByVal asg As DataGridView, ByVal kodeBrg As String)

        Dim dt As New DataTable
        Dim filter As String

        filter = ""
        Try
            adapter = New MySqlDataAdapter(
                "select 0, kode_bb, b.nama, qty from detil_spesifikasi_barang d, master_bahan_baku b where kode_brg = '" & kodeBrg & "'" &
                " and d.kode_bb=b.kode", Me.Conn)
            adapter.Fill(dt)
            For i As Integer = 0 To dt.Rows.Count - 1
                asg.Rows.Add()
                asg(0, i).Value = i + 1
                asg(1, i).Value = dt.Rows(i)(1).ToString()
                asg(2, i).Value = dt.Rows(i)(2).ToString()
                asg(3, i).Value = dt.Rows(i)(3).ToString()
            Next

        Catch ex As Exception
            MessageBox.Show(ex.Message)

        Finally
            Me.Conn.Close()
        End Try
    End Sub

    Public Function get_auto_increment_seq_from_table(ByVal nmTabel As String) As Integer
        Dim dt As New DataTable

        Try
            adapter = New MySqlDataAdapter(
                    "Select Case AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'sql12384047' " &
                    " And TABLE_NAME = '" & nmTabel & "'", Me.Conn)
            adapter.Fill(dt)

            Return dt.Rows(0)(0).ToString()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return 0
        End Try
    End Function
    Public Sub save_permintaan_pembelian_master(ByVal Tanggal As String, ByVal Nomor As String, ByVal Ket As String, ByVal myCommand As MySqlCommand)

        myCommand.CommandText = "insert into permintaan_pembelian_master (Tanggal,Nomor, keterangan) values (" _
                 & "str_to_date('" & Tanggal & "', '%m/%d/%Y'),'" _
                 & Nomor & "','" _
                 & Ket & "'" _
                 & ")"
        myCommand.ExecuteNonQuery()



    End Sub

    Public Sub update_permintaan_pembelian_master(ByVal seq As String, ByVal Tanggal As String, ByVal Nomor As String, ByVal Ket As String, ByVal myCommand As MySqlCommand)

        myCommand.CommandText = "update permintaan_pembelian_master set Tanggal = str_to_date('" & Tanggal & "', '%m/%d/%Y')," _
                 & " Nomor = '" & Nomor & "'," _
                 & " keterangan = '" & Ket & "'" _
                 & " where seq = '" & seq & "'"
        myCommand.ExecuteNonQuery()
    End Sub

    Public Sub delete_permintaan_pembelian_master(ByVal seq As String)
        eksekusi("delete permintaan_pembelian_master where seq = '" & seq & "'", "")
    End Sub

    Public Function get_nomor_all_trans(ByVal tipeTrans As String, ByVal tgl As String) As String

        Dim dt As New DataTable
        Dim sql As String
        Try
            sql = "Select concat('" & tipeTrans & "-',DATE_FORMAT('" & tgl & "', '%d%m%y'),LPAD(" _
                    & "(select 1+COALESCE(max(RIGHT(nomor, 4)),0) from permintaan_pembelian_master where (left(nomor,9)) = (concat('" & tipeTrans & "-',DATE_FORMAT('" & tgl & "', '%d%m%y'))))" _
                    & ",4,'0')) "
            adapter = New MySqlDataAdapter(sql, Me.Conn)
            adapter.Fill(dt)

            Return dt.Rows(0)(0).ToString()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return ""
        End Try
    End Function


    Public Function get_date_now() As String

        Dim dt As New DataTable

        Try
            adapter = New MySqlDataAdapter(
                    "Select NOW() ", Me.Conn)
            adapter.Fill(dt)

            Return dt.Rows(0)(0).ToString()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return ""
        End Try
    End Function


    Public Sub load_rekap_permintaan_pembelian_master(ByVal asg As DataGridView, ByVal TglDari As String, ByVal TglSampai As String, ByVal kodeBB As String)

        Dim dt As New DataTable
        Dim filter As String

        filter = ""
        kodeBB = Trim(kodeBB)
        If kodeBB <> "" Then
            filter = " and d.kode_bb = '" & kodeBB & "'"
        End If

        filter = filter + " and m.tanggal <= '" & TglDari & "' and m.tanggal >= '" & TglSampai & "'"
        Try
            adapter = New MySqlDataAdapter("select m.seq ,0, tanggal, nomor, m.keterangan from permintaan_pembelian_master m, permintaan_pembelian_detail d " _
                                           & " where m.seq = d.master_seq  " & filter, Me.Conn)
            adapter.Fill(dt)
            asg.DataSource = dt
            'Mendapatkan BindingManagerBase
            asg.Columns(0).HeaderText = "Seq"
            asg.Columns(1).HeaderText = "No."
            asg.Columns(2).HeaderText = "Tanggal"
            asg.Columns(3).HeaderText = "Nomor"
            asg.Columns(4).HeaderText = "Keterangan"

            asg.Columns(0).Visible = False

            For i As Integer = 0 To asg.Rows.Count - 1
                asg(1, i).Value = i + 1
            Next

        Catch ex As Exception
            MessageBox.Show(ex.Message)

        Finally
            Me.Conn.Close()
        End Try
    End Sub



    Public Sub save_permintaan_pembelian_detail(ByVal master_seq As String, ByVal kode_bb As String, ByVal qty As Double, ByVal Ket As String, ByVal myCommand As MySqlCommand)

        myCommand.CommandText = "insert into permintaan_pembelian_detail (master_seq, kode_bb, qty, keterangan) values ('" _
                 & master_seq & "','" _
                 & kode_bb & "'," _
                 & qty & ",'" _
                 & Ket & "'" _
                 & ")"
        myCommand.ExecuteNonQuery()
    End Sub

    Public Sub delete_permintaan_pembelian_detail(ByVal master_seq As String, ByVal myCommand As MySqlCommand)
        myCommand.CommandText = "delete from permintaan_pembelian_detail where master_seq = '" & master_seq & "'"
        myCommand.ExecuteNonQuery()
    End Sub


    Public Sub load_permintaan_pembelian_detail(ByVal asg As DataGridView, ByVal mstSeq As String)

        Dim dt As New DataTable
        Dim filter As String

        filter = ""
        Try
            adapter = New MySqlDataAdapter(
                "select 0, kode_bb, b.nama, qty from permintaan_pembelian_detail d, master_bahan_baku b where master_seq = '" & mstSeq & "'" &
                " and d.kode_bb=b.kode", Me.Conn)
            adapter.Fill(dt)
            For i As Integer = 0 To dt.Rows.Count - 1
                asg.Rows.Add()
                asg(0, i).Value = i + 1
                asg(1, i).Value = dt.Rows(i)(1).ToString()
                asg(2, i).Value = dt.Rows(i)(2).ToString()
                asg(3, i).Value = dt.Rows(i)(3).ToString()
            Next

        Catch ex As Exception
            MessageBox.Show(ex.Message)

        Finally
            Me.Conn.Close()
        End Try
    End Sub

End Class
