﻿Imports MySql.Data.MySqlClient
Public Class frmRekapUser
    Dim adapter As MySqlDataAdapter

    Sub setAlignColumn()

        asgRekapUser.Columns(0).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekapUser.Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekapUser.Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter
        asgRekapUser.Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.TopCenter

        asgRekapUser.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

    End Sub
    Public Sub load()
        frmLogin.Koneksi.load_rekap_user(asgRekapUser, Trim(txtCari.Text))

    End Sub
    Private Sub frmRekapUser_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        frmLogin.Koneksi.load_rekap_user(asgRekapUser, Trim(txtCari.Text))
        setAlignColumn()
    End Sub

    Public Const WM_NCLBUTTONDBLCLK As Integer = &HA3

    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
        If m.Msg = WM_NCLBUTTONDBLCLK Then Return
        MyBase.WndProc(m)
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        frmAddUser.userid = ""
        frmAddUser.ShowDialog()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        frmLogin.Koneksi.load_rekap_user(asgRekapUser, Trim(txtCari.Text))
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        frmAddUser.userid = asgRekapUser(1, asgRekapUser.CurrentCell.RowIndex).Value
        frmAddUser.ShowDialog()
    End Sub

    Private Sub btnHapus_Click(sender As Object, e As EventArgs) Handles btnHapus.Click
        Dim result As DialogResult = MessageBox.Show("Apakah user '" & asgRekapUser(1, asgRekapUser.CurrentCell.RowIndex).Value & "' ingin dihapus?",
                              "Hapus",
                              MessageBoxButtons.YesNo)
        If (result = DialogResult.Yes) Then
            frmLogin.Koneksi.delete_user(asgRekapUser(1, asgRekapUser.CurrentCell.RowIndex).Value)
            load()
        End If
    End Sub
End Class